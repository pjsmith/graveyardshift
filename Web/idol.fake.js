var grave = require('./grave.js');
var _ = require('lodash');
var check = require('check-types');
var cemetery = require('./cemetery.js');
var faker = require('faker');

var _id = 1;

function pickone(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

function synthesizeGravePrecis(id) {

    function nextId() {
        return ++_id;
    }

    function generatePlotCode() {
        var plotCode = pickone(['A', 'B', 'C', 'D']) + pickone([1, 2, 3, 4]);
        return plotCode;
    }
        
    return new grave.GravePrecis(id || nextId(), faker.name.firstName(), faker.name.lastName(), generatePlotCode(), nextId());
}

function synthesizeGraveData(id) {
    var precis = synthesizeGravePrecis(id);
    return _.extend(precis,
    {
        epitaph: faker.lorem.sentences(2),
        imageUrl: faker.image.imageUrl(),
        freeData: faker.lorem.paragraph()
    });
}

module.exports = {  
    search: function (parameters, callback) {
        if (check.emptyObject(parameters)) {
            throw "search: id must be a non-null object";
        }
        var result = _.range(1, 100).map(function () { return synthesizeGravePrecis(); });
        callback(result);
    },
    getGraveData: function (id) {
        if (!check.number(id)) {
            throw "getGraveData: id must be a number";
        }
        return synthesizeGraveData(id);
    },
    getCemeteryData: function(id) {
        if (!check.number(id)) {
            throw "getCemeteryData: id must be a number";
        }
        var city = faker.address.city();
        var fakeAddress = [faker.address.streetAddress(), city].join();

        return new cemetery.Cemetery(id, city + ' Cemetery', fakeAddress, faker.phone.phoneNumber());
    }
};