var _isFake = false;

module.exports = {
    fake: function(isFake) {
        _isFake = isFake;
    },
    isFake: function () { return _isFake; },
    port: 56456,
    idolApiKey: '9dd2c020-6edd-45f7-b93b-28e3ac9279b3',
    idolHost: 'https://api.idolondemand.com',
    idolIndexes: ['cemeterydata']
}