var request = require('request');
var q = require('q');
var grave = require('./grave.js');
var billy = require('./billy.js');
var config = require('./config.js');
var check = require('check-types');

function getGraveData(graveId) {
    if (!check.number(graveId)) {
        throw "Grave ID must be a number";
    }
    
    return {
        
    }
}

function search(searchParams, callback) {
    var searchText = searchParams.text;
    var idol = new billy.IDOLClient(config.idolHost, config.idolApiKey);
    idol.queryTextIndex().literalQuery(searchText, config.idolIndexes   , callback);
}
    
function getCemeteryData(cemeteryId) {
    
}

module.exports = {
    search: search,
    getGraveData: getGraveData
}