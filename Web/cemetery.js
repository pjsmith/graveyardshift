module.exports = {
    Cemetery: function (id, name, location, phoneNumber) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.phoneNumber = phoneNumber;
    }
};