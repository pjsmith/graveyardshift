var request = require('request');
var _ = require('lodash');
var url = require('url');
var Q = require('q');

function createApiFunctionUrl(idolApiUrlRoot, apiFunction) {
    return [idolApiUrlRoot, 1, 'api', 'sync', apiFunction, 'v1'].join('/');
}

function IDOLClient(apiUrl, apiKey) {
    this.apiUrl = apiUrl;
    this.apiKey = apiKey;
}

IDOLClient.prototype.get = function(apiFunction, parameters, callback) {
    var queryUrl = createApiFunctionUrl(this.apiUrl, 'querytextindex');

    _.extend(parameters, {
        apikey: this.apiKey
    });

    var target = url.parse(queryUrl);
    target.query = parameters;  
    console.log('started IDOL query');
    return request(url.format(target), function (err, response, body) {
        console.log('completed IDOL query');
        callback(err, response, body);
    });
}

IDOLClient.prototype.queryTextIndex = function () {
    return new TextIndexQueryAgent(this);
}

function TextIndexQueryAgent(client) {
    this.endpointName = '';
    this.client = client;
}

TextIndexQueryAgent.prototype.literalQuery = function (queryString, indexes, callback) {

    var params = {
        print: 'all',
        text: queryString,
        total_results: true,
        absolute_max_results: 1000
    };

    if (indexes instanceof Array && indexes.length > 0) {
        params.indexes = indexes;
    }

    return this.client.get(this.endpointName, params, function(err, response, body) {
        console.log('literalQuery callback: ' + callback);
        callback(body);
    });
}

module.exports = {
    IDOLClient: IDOLClient
};