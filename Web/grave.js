var _ = require('lodash');

function GravePrecis(id, givenNames, surname, plotCode, cemeteryId, died, interred) {
    this.id = id;
    this.givenNames = givenNames;
    this.surname = surname;
    this.occupantName = this.givenNames + ' ' + this.surname;
    this.plotCode = plotCode;
    this.cemeteryId = cemeteryId;
    this.died = died;
    this.interred = interred;
}

function GraveData(id, givenNames, surname, plotCode, cemeteryId, died, interred, epitaph, imageUrl, freeData) {
    return _.extend(new GravePrecis(id, givenNames, surname, plotCode, cemeteryId, died, interred),
        {
            epitaph: epitaph,
            imageUrl: imageUrl,
            freeData: freeData
        }
    );
}

module.exports = {
    GravePrecis: GravePrecis,
    GraveData: GraveData
};