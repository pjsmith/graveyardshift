var express = require('express');
var config = require('./config.js');
var idolGatewayFac = function() {
    return config.isFake() ? require('./idol.fake.js') : require('./idol.js');
};
var url = require('url');
var check = require('check-types');
var _ = require('lodash');

var app = express();
var graveRouter = express.Router();

graveRouter.get('/search', function(req, res) {
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;
    idolGatewayFac().search(query, function (candidates) {
        console.log('serving search');
        var cleanCandidates = cleanJson(candidates);
        serveJson(res, cleanCandidates);
    });
});

graveRouter.get('/:id', function (req, res) {
    var id = parseInt(req.params.id);
    var grave = idolGatewayFac().getGraveData(id);
    serveJson(res, grave);  
});

var cemeteryRouter = express.Router();
cemeteryRouter.get('/:id', function (req, res) {
    var id = parseInt(req.params.id);
    serveJson(res, idolGatewayFac().getCemeteryData(id));
});

var configRouter = express.Router();
configRouter.post('/fake', function (req, res) {
    console.log('entered fake handler');
    var urlParts = url.parse(req.url, true);
    var query = urlParts.query;
    if (query.fake === 'true' || query.fake === 'false') {
        config.fake(query.fake === 'true');
    }
    res.end('IDOL implementation is ' + (config.isFake() ? 'fake' : 'real'));
});

function serveJson(res, obj) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(JSON.stringify(obj));
}

function convertIdolResultToCleanJson(dirtyResult) {
    console.log('dirty result: ' + JSON.stringify(dirtyResult));
    var content = dirtyResult.content;
    var output = {};
    output.id = dirtyResult.reference;
    output.weight = dirtyResult.weight;
    console.log('content: ' + content);
    var unwrappedContent = content.replace(/\{/g, '').replace(/\}/g, '');
    console.log('unwrappedcontent: ' + JSON.stringify(unwrappedContent))
    console.log('unwrappedcontent is string? ' + (unwrappedContent instanceof String))
    var sections = unwrappedContent.split(',');

    for (var i = 0; i < sections.length; i++) {
        var sectionHeader = sections[i].split(':')[0].replace(/\s+/g, '');
        if (sectionHeader && !/^\s*$/g.test(sectionHeader)) {
            output[sectionHeader] = sections[i].split(':')[1];
        }
    }
    return output;
}

function cleanJson(jsonText) {
    console.log(jsonText);
    if (check.string(jsonText)) {
        var results = JSON.parse(jsonText).documents;
        console.log('results array : ', results);
        return _.map(results, convertIdolResultToCleanJson);
    } else {
        return jsonText;
    }
}
app.use('/config', configRouter);
app.use('/grave', graveRouter);
app.use('/cemetery', cemeteryRouter);


var server = app.listen(process.env.VCAP_APP_PORT || 56456);
/*var server = app.listen(config.port, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Listening on http://%s:%s', host, port);
});*/

