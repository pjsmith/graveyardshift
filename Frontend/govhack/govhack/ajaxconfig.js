﻿define(['flashMessages', 'spinner'], function(flashMessages) {

    // Automatically cancel unfinished ajax requests 
    // when the user navigates elsewhere.
    // c.f. http://stackoverflow.com/questions/1802936/stop-all-active-ajax-requests-in-jquery

    function setupAjaxAutoCancellation() {
        var xhrPool = [];
        $(document).ajaxSend(function (e, jqXhr, options) {
            xhrPool.push(jqXhr);
        });
        $(document).ajaxComplete(function (e, jqXhr, options) {
            xhrPool = $.grep(xhrPool, function (x) { return x !== jqXhr });
        });
        var abort = function () {
            $.each(xhrPool, function (idx, jqXhr) {
                jqXhr.abort();
            });
        };

        var oldbeforeunload = window.onbeforeunload;
        window.onbeforeunload = function () {
            var r = oldbeforeunload ? oldbeforeunload() : undefined;
            if (r == undefined) {
                // only cancel requests if there is no prompt to stay on the page
                // if there is a prompt, it will likely give the requests enough time to finish
                abort();
            }
            return r;
        }
    };

    setupAjaxAutoCancellation();
});