﻿
requirejs.config({
	paths: {
		jquery: "/resources/scripts/jquery-2.1.4.min",
		knockout: "/resources/scripts/knockout",
		common: "/resources/scripts/common",
		
		text: "/resources/scripts/text",

		kendo: "/resources/scripts/kendo.all.min",
		kokendo: "/resources/scripts/knockout-kendo.min",
		
		postbox: "/resources/scripts/knockout-postbox",
		dataModule: "./dataModule"
	},
	shim: {
	},

});

require(["jquery",
	"knockout",
	"dataModule",
	"postbox",
    "main"],
	function ($, ko, dataModule, postbox) {
		ko.components.register("grid", {
			viewModel: { require: 'components/grid.js'},
			template: { require: "text!components/grid.html" }
		});

		ko.components.register("search", {
			viewModel: { require: 'components/search.js' },
			template: { require: "text!components/search.html" }
		});

		this.gridChannel = "gridChannel";
		this.searchChannel = "searchChannel";

		ko.postbox.subscribe(this.searchChannel, function (data) {
			console.log("Init.js -> passing search to dataModule.");
			var results = dataModule.search(data, bindGridData);
			
			//
		})

		function bindGridData(data) {
			console.log("Init.js -> Passing results to grid.");
			ko.postbox.publish(this.gridChannel, data);
		}

		this.clickTest = function () {
			ko.postbox.publish(this.gridChannel, dataModule.generateData());
		}

		ko.applyBindings();
	}
);
