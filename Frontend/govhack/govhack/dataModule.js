﻿define(['idol', 'appconfig'], function (idol, appconfig){

	var idolClient = new idol.IDOL(appconfig.apiRoot);
	var self = this;
	var search = function (searchText, onSuccess, onFail) {
		console.log("dataModule -> searching idol with: " + searchText);
		idolClient.search(searchText,
			function (searchResults) {
				console.log("dataModule -> results returned");
				onSuccess(searchResults);
			}
		);
	}
	

	//var dataModule = {};
	//var generateData = function () {
	//	var testData = [];
	//	var length = 100;

	//	function randomString(min, max) {
	//		var length = Math.floor(Math.random() * max);
	//		length += length > min ? 0 : (min - length);
	//		var str = "";
	//		var alpha = "abcdefghijklmnopqrstuvwxyz";
	//		for (var i = 0; i < length; i++) {
	//			str += alpha.charAt(Math.floor(Math.random() * alpha.length));
	//		}
	//		return str;
	//	}

	//	for (var i = 0; i < length; i++) {
	//		testData.push({ FirstName: randomString(3, 10), LastName: randomString(3, 10), Age: Math.floor(Math.random() * 100) });
	//	}
	//	return testData;
			
	return {
		search: search
	};
});