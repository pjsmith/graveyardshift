﻿define(['jquery', 'flashMessages', 'appconfig'], function ($, flashMessages) {
    
    function IDOL(rootUrl) {
        this.rootUrl = rootUrl;
    }

    IDOL.prototype.search = function(searchString, onSuccess) {
        $.ajax({
            method: 'GET',
            url: this.rootUrl + '/grave/search?text=' + searchString,
            success: function(data, textStatus, xhr) {
                onSuccess(data);
            },
            error: function() {
                flashMessages.error("Oops! Something went wrong with your search. Why don't you try again?");
            }
        });
    }

    return {
        IDOL: IDOL
    };
});