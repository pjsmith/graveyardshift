﻿define(['jquery', 'knockout', 'kokendo', 'postbox'],
	function ($, ko, kokendo, dataModule, postbox) {
		function grid(params) {
			this.dataLoaded = ko.observable(false);
			this.items = ko.observableArray();

			//	if new data on pb_data parse and refresh
			ko.postbox.subscribe(params.inputChannel || "",
				function (data) {
					console.log("Data received by grid.js");
					this.dataLoaded(false);
					this.parseData(data).bindNewData();
				}.bind(this))
		}

		grid.prototype.parseData = function (data) {
			this.data = data;
			return this;
		}

		grid.prototype.bindNewData = function () {
			this.dataLoaded(true);
			this.items(this.data);
		}

		return grid;
	}
);