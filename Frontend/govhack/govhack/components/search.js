﻿define(['jquery', 'knockout'], function ($, ko) {
	function search(params) {
		this.outputChannel = params.outputChannel || "";
		this.searchText = ko.observable();
	}

	

	search.prototype.searchClick = function ($elt, getSearchText, processResult) {
		console.log("Search.js -> search for: " + getSearchText);
		ko.postbox.publish(this.outputChannel, this.searchText());


		//$elt.click(function (e) {
		//	
		//	var $parent = $(this);
		//	var searchText = getSearchText();
		//	e.preventDefault();
		//	var idolClient = new idol.IDOL(appconfig.apiRoot);

		//	idolClient.search(searchText, function (searchResults) {
		//		processResult.call($parent, searchResults, searchText);
		//	});
		//});
	}
	return search;
});