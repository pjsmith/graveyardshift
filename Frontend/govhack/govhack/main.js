﻿define([
	"jquery",
	"knockout",
	"dataModule",
	"postbox",
	"idol",
	"appconfig",
	"flashMessages",
    "spinner",
	"ajaxconfig"
], function ($, ko, dataModule, postbox, idol, appconfig, flashMessages, spinner){
	function main() {

		

		ko.applyBindings();

		//setTimeout(function () {
		//	ko.postbox.publish(this.gridChannel, "testValue");
		//}, 1000);

		this.initialize = function () {
			
		}
	}

	//var processSearchResults = function(results, searchText) {
	//    var $parentElement = $(this);
	//    $('.json-results').remove();
	//    $parentElement.after($('<pre class="json-results">' + JSON.stringify(results, null, 2) + '</pre>'));
	//    $parentElement.after($('<p>Your search: ' + searchText + '</p>'));
	//}

    flashMessages.flasher($('ul.flash-messages'));
	spinner.spinner($('.spinner'));
    //search.searchClick($('a#search-button'), function () {
    //	return $('input#search').val();
    //}, processSearchResults);

	return main;
})