﻿define(['jquery'], function($) {

    var $flasher = undefined;

    function flasher($elt) {
        $flasher = $elt;
        $flasher.empty();
    }

    function clear() {
        if ($flasher) {
            $flasher.empty();
        }
    }

    function success(messageText) {
        clear();
        if ($flasher) {
            $flasher.append($('<li class="flash-success">' + messageText + '</li>'));
        }
    }

    function error(messageText) {
        clear();
        if ($flasher) {
            $flasher.append($('<li class="flash-error">' + messageText + '</li>'));
        }
    }

    $(document)
        .on("ajaxSend", function () {
            if ($flasher) {
                clear();
            }
        });

    return {
        flasher: flasher,
        clear: clear,
        success: success,
        error: error
    };
});