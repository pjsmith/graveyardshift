﻿define(['jquery'], function() {

    var spinnerImage = '/resources/images/ajax-loader.gif';
    
    function show($elt) {
        $elt.css('background-image', 'url(' + spinnerImage + ')');
    }

    function hide($elt) {
        $elt.css('background-image', '');
    }
    function spinner($elt) {
        
        hide($elt);

        $(document)
            .on("ajaxSend", function () {
                show($elt);
            })
            .on("ajaxStop", function () {
                hide($elt);
            })
            .on("ajaxError", function () {
                hide($elt);
            });
    }

    return {
        spinner: spinner
    };
});